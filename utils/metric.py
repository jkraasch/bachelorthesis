import numpy as np
from tensorflow.keras import layers


# Metic with sliced wasserstein
# taken from original proGAN implementation and generalized for videos
# https://github.com/tkarras/progressive_growing_of_gans/blob/master/metrics/sliced_wasserstein.py
def get_descriptors_for_minibatch(minibatch, nhood_size=4,
                                  nhoods_per_image=32):  # --> random window samples from image as descriptor
    S = minibatch.shape  # (minibatch, frames,height, width)
    assert len(S) == 5
    N = nhoods_per_image * S[0]
    H = nhood_size // 2

    nhood, frames, x, y, chan = np.ogrid[0:N, 0:S[1], -H:H + 1, -H:H + 1,
                                0:1]  # shape--> N1111, 1N111, 11N11, 111N1, 1111N

    img = nhood // nhoods_per_image  # descriptor num_desc * batchsize -> for every image one descriptor

    x = x + np.random.randint(H, S[3] - H, size=(N, 1, 1, 1, 1))
    y = y + np.random.randint(H, S[2] - H, size=(N, 1, 1, 1, 1))

    idx = ((((img + chan) * S[1] + frames) * S[2] + y) * S[3] + x)  # --> convert multidimensional index to 1D index
    return minibatch.flat[idx]


def finalize_descriptors(desc):
    # normalizing the descriptors
    if isinstance(desc, list):
        desc = np.concatenate(desc, axis=0)
    assert desc.ndim == 5  # (neighborhood, frames, height, width,channels)
    desc -= np.mean(desc, axis=(0, 2, 3, 4), keepdims=True)
    desc /= np.std(desc, axis=(0, 2, 3, 4), keepdims=True) + 1e-08
    desc = desc.reshape(desc.shape[0], -1)
    return desc


def sliced_wasserstein(A, B, dir_repeats, dirs_per_repeat):
    assert A.ndim == 2 and A.shape == B.shape  # (neighborhood, descriptor_component)
    results = []
    for repeat in range(dir_repeats):
        dirs = np.random.randn(A.shape[1], dirs_per_repeat)  # (descriptor_component, direction)
        dirs /= np.sqrt(
            np.sum(np.square(dirs), axis=0, keepdims=True))  # normalize descriptor components for each direction
        dirs = dirs.astype(np.float32)
        projA = np.matmul(A, dirs)  # (neighborhood, direction)
        projB = np.matmul(B, dirs)
        projA = np.sort(projA, axis=0)  # sort neighborhood projections for each direction
        projB = np.sort(projB, axis=0)
        dists = np.abs(projA - projB)  # pointwise wasserstein distances
        results.append(np.mean(dists))  # average over neighborhoods and directions
    return np.mean(results)


def laplacian_pyramid(videos, lods):
    # laplacian pyramid approximated by difference of blurred images (approx. DoG)
    pyramid = []
    for lod in lods:
        down_sampled = layers.AveragePooling3D((2, 2, 2), padding="same")(videos)
        blurred = layers.UpSampling3D((2, 2, 2))(down_sampled)
        pyramid.append(videos - blurred)
        videos = down_sampled

    return pyramid


def compute_metric(reals, fakes, reses, nhoods):
    # function to use to compare two distributions

    dir_repeats = 4
    dirs_per_repeat = 128

    # compute laplacian pyramids
    pyr_real = laplacian_pyramid(reals, reses)
    pyr_fake = laplacian_pyramid(fakes, reses)

    # sample descriptors
    desc_real = []
    for level in pyr_real:
        desc_real.append(get_descriptors_for_minibatch(level.numpy(), nhoods))
    desc_fake = []
    for level in pyr_fake:
        desc_fake.append(get_descriptors_for_minibatch(level.numpy(), nhoods))
    fin_real = []
    fin_fake = []
    for desc in desc_real:
        fin_real.append(finalize_descriptors(desc))

    for desc in desc_fake:
        fin_fake.append(finalize_descriptors(desc))

    # compute SWD between distributions
    w_dists = []
    for r_d, f_d in zip(fin_real, fin_fake):
        w_dists.append(sliced_wasserstein(r_d, f_d, dir_repeats, dirs_per_repeat))

    return w_dists
