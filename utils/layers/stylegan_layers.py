from tensorflow.keras.layers import Layer
from tensorflow.keras import layers
from tensorflow.keras import backend as K
import tensorflow as tf


@tf.function
def constant_input(batch):
    """
    Constant Input used for the generator
    @param batch: give the current batch of latent vectors, to compute shape
    @return: constant input vector
    """
    const = tf.ones([1, 100])
    batch_size = tf.keras.backend.shape(batch)[0]
    tiled_const = tf.keras.backend.tile(const, [batch_size, 1])
    return tiled_const


class Noise(tf.keras.layers.Layer):
    """
    Noise injections:
    Create uniform noise and then add a Dense layer so it is trainable
    """
    def __init__(self, **kwargs):
        super(Noise, self).__init__(**kwargs)

    def get_config(self):
        config = super().get_config().copy()
        return config

    def build(self, input_shape):
        self.noise = tf.random.uniform([input_shape[1], input_shape[2], input_shape[3], 1])
        self.dense = layers.Dense(input_shape[4])(tf.ones([1, input_shape[4]]))
        self.reshape = tf.reshape(self.dense, [1, 1, 1, -1])

    @tf.function
    def call(self, x):
        return x + self.noise * self.reshape


# Input b and g should be 1x1xC
class AdaInstanceNormalization(Layer):
    """
    Adaptive Instance Normalization as described in the styleGAN paper
    inspired by https://github.com/manicman1999/StyleGAN-Keras/blob/master/AdaIN.py
    """

    def __init__(self,
                 **kwargs):
        super(AdaInstanceNormalization, self).__init__(**kwargs)

    def call(self, inputs, training=None):
        input_shape = K.int_shape(inputs[0])
        reduction_axes = list(range(0, len(input_shape)))

        #get the mappings from the style vector to bias and scale
        bias = inputs[1]
        scale = inputs[2]

        del reduction_axes[-1]
        del reduction_axes[0]

        # Normalize Input
        mean = K.mean(inputs[0], reduction_axes, keepdims=True)
        stddev = K.std(inputs[0], reduction_axes, keepdims=True) + 1e-3
        normed = (inputs[0] - mean) / stddev

        # Scale and bias normalized inputs
        return normed * (scale+1) + bias  # in stylegan on github they do (scale+1)

    def get_config(self):
        # for serialization of layer
        base_config = super(AdaInstanceNormalization, self).get_config()
        return dict(list(base_config.items()))