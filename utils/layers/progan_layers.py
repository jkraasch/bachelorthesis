import tensorflow as tf
from tensorflow.keras import backend
from tensorflow.python.ops import gen_math_ops
from tensorflow.python.ops import array_ops, standard_ops, math_ops, sparse_ops, nn
from tensorflow_core.python.keras.backend import is_sparse


class PixelNorm(tf.keras.layers.Layer):
    """
    Pixel Normalization as described in the thesis
    """

    def __init__(self, **kwargs):
        super(PixelNorm, self).__init__(**kwargs)

    def get_config(self):
        config = super().get_config().copy()

    def call(self, activations):
        # shape: (Batch, x, y, fmaps) --> normalize regarding the feature maps axis
        ax = len(activations.shape) - 1
        return activations * tf.math.rsqrt(
            tf.keras.backend.mean(tf.keras.backend.square(activations), axis=ax, keepdims=True) + 1e-08)


# mini-batch standard deviation layer generalized to videos
# image version taken from:
# https://machinelearningmastery.com/how-to-train-a-progressive-growing-gan-in-keras-for-synthesizing-faces/
class MinibatchStdev(tf.keras.layers.Layer):
    # initialize the layer
    def __init__(self, **kwargs):
        super(MinibatchStdev, self).__init__(**kwargs)

    # perform the operation
    def call(self, image, layer):
        # calculate the mean value for each pixel across channels
        mean = backend.mean(image, axis=0, keepdims=True)
        # calculate the squared differences between pixel values and mean
        squ_diffs = backend.square(image - mean)
        # calculate the average of the squared differences (variance)
        mean_sq_diff = backend.mean(squ_diffs, axis=0, keepdims=True)
        # add a small value to avoid a blow-up when we calculate stdev
        mean_sq_diff += 1e-8
        # square root of the variance (stdev)
        stdev = backend.sqrt(mean_sq_diff)
        # calculate the mean standard deviation across each pixel coord
        mean_pix = backend.mean(stdev, keepdims=True)
        # scale this up to be the size of one input feature map for each sample
        shape = backend.shape(layer)
        output = backend.tile(mean_pix, (shape[0], shape[1], shape[2], shape[3], 1))
        # concatenate with the output
        combined = backend.concatenate([layer, output], axis=-1)
        return combined


class EqlConv3D(tf.keras.layers.Conv3D):
    """
    Normalize weights so that they are updated according to their importance
    """

    def __init__(self, *args, **kwargs):
        super(EqlConv3D, self).__init__(*args, **kwargs)

    def get_config(self):

        config = super().get_config().copy()

        return config

    def call(self, inputs):
        input_shape = inputs.shape
        fan_in = tf.reduce_prod(input_shape[1:])
        std = tf.sqrt(tf.constant(2.0, name='gain') / tf.dtypes.cast(fan_in, tf.float32))
        outputs = self._convolution_op(inputs, self.kernel * std)
        if self.use_bias:
            if self.data_format == 'channels_first':
                if self.rank == 1:
                    # nn.bias_add does not accept a 1D input tensor.
                    bias = array_ops.reshape(self.bias, (1, self.filters, 1))
                    outputs += bias
                else:
                    outputs = nn.bias_add(outputs, self.bias, data_format='NCHW')
            else:
                outputs = nn.bias_add(outputs, self.bias, data_format='NHWC')

        if self.activation is not None:
            return self.activation(outputs)
        return outputs


class EqlDense(tf.keras.layers.Dense):
    """
    Normalize weights so that they are updated according to their importance
    """

    def __init__(self, *args, **kwargs):
        super(EqlDense, self).__init__(*args, **kwargs)

    def get_config(self):

        config = super().get_config().copy()

        return config

    def call(self, inputs):
        input_shape = inputs.shape
        fan_in = tf.reduce_prod(input_shape[1:])
        std = tf.sqrt(tf.constant(2.0, name='gain') / tf.dtypes.cast(fan_in, tf.float32))
        rank = len(inputs.shape)
        if rank > 2:
            # Broadcasting is required for the inputs.
            outputs = standard_ops.tensordot(inputs, self.kernel * std, [[rank - 1], [0]])
        else:
            inputs = math_ops.cast(inputs, self._compute_dtype)
            if is_sparse(inputs):
                outputs = sparse_ops.sparse_tensor_dense_matmul(inputs, self.kernel * std)
            else:
                outputs = gen_math_ops.mat_mul(inputs, self.kernel * std)
        if self.use_bias:
            outputs = nn.bias_add(outputs, self.bias)
        if self.activation is not None:
            return self.activation(outputs)  # pylint: disable=not-callable
        return outputs
