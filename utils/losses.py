import tensorflow as tf


# Define the loss for the generator.
def discriminator_loss(d_real, d_fake):
    loss = tf.reduce_mean(d_fake) - tf.reduce_mean(d_real)
    return loss


# Define the loss for the discriminator.
def generator_loss(outputs):
    loss = -tf.reduce_mean(outputs)
    return loss


def D_wgan_gp(loss, D, fakes, minibatch_size, reals, lod, alpha, transitioning,
              wgan_lambda=10.0,
              wgan_target=1.0):
    """
    WGAN-GP gradient penalty inspired by https://github.com/NVlabs/stylegan/blob/master/training/loss.py
    @param loss: discriminator loss to be penalized
    @param D: Discriminator
    @param fakes: batch of fakes
    @param minibatch_size: size of the batch
    @param reals: batch of reals
    @param lod: resolution that is worked in
    @param alpha: weighting factor if the model is transitioning from resolution to resolution
    @param transitioning: boolean of whether the model is transioning to a new resolution
    @param wgan_lambda:  Weight for the gradient penalty term
    @param wgan_target: Target value for gradient magnitudes
    @return: penalized loss
    """

    with tf.name_scope('GradientPenalty'):
        mixing_factors = tf.random.uniform([minibatch_size, 1, 1, 1, 1], 0.0, 1.0, dtype=fakes.dtype)
        reals = tf.cast(reals, fakes.dtype)
        # Line of samples between real and fake
        mixed_images_out = reals + (fakes - reals) * mixing_factors
        with tf.GradientTape() as penalizer:
            # compute loss and gradients
            penalizer.watch(mixed_images_out)
            mixed_scores_out = D(mixed_images_out, lod, alpha, transitioning)
            mixed_loss = tf.reduce_sum(mixed_scores_out)
            mixed_grads = penalizer.gradient(mixed_loss, [mixed_images_out])[0]
            # compute gradient norm
            mixed_norms = tf.sqrt(tf.reduce_sum(tf.square(mixed_grads), axis=[1, 2, 3, 4]))
            # compute penalty term
            gradient_penalty = tf.reduce_mean(tf.square(mixed_norms - wgan_target))

    # penalize loss
    loss += gradient_penalty * (wgan_lambda / (wgan_target ** 2))
    return loss
