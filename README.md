# Bachelor Thesis

This is the code  that accompanies the Bachelor Thesis of Jonas Kraasch

main.py starts a training run that took approx. 1 hour on google colab (right now it stores a gif every 100 steps but the code can be changed to plot the frames next to eachother insted)

in the generator/discriminator, you can change the layer-blocks to your liking

to run the code you need tensorflow 2.x and tensorflow-datasets in addition to numpy and matplotlib

animate.py is used to plot the samples stored by the training process (sampled every 10 minutes)

