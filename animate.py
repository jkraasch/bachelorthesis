import matplotlib.pyplot as plt
import pickle
import numpy as np
import tensorflow as tf

trans = pickle.load(open("trans.pkl", "rb"))


#plt.figure(figsize=(20, 5))
for j, sequences in enumerate(trans[-3:]):
    sequence = sequences[0]
    sequence = np.squeeze(sequence, axis=-1)

    print(sequence.shape)
    for i, img in enumerate(sequence):
        plt.subplot(len(trans), len(sequence), j* len(sequence) + i + 1)
        plt.imshow(img, cmap='gray')
        plt.axis('off')
        plt.tight_layout()
plt.show()
