import pickle
import time
import matplotlib.pyplot as plt

import tensorflow_datasets as tfds
from tensorflow_datasets.video import moving_sequence

from models.discriminator import *
from models.generator import *
from utils.losses import *
from utils.metric import compute_metric

# Loading the dataset from tensorflow_datasets
mnist_ds = tfds.load('mnist', split=tfds.Split.TRAIN, as_supervised=True, shuffle_files=True)
mnist_ds = mnist_ds.repeat().shuffle(1024)


# Preprocessing of the dataset
def map_fn(image, label):
    # turn samples from tfds to sequences to train on
    sequence = moving_sequence.image_as_moving_sequence(image, sequence_length=32, output_size=(64, 64))
    return (sequence.image_sequence)


def fun(image):
    # Downsampling so the dataset has shape (32,64,64)
    return layers.AveragePooling3D()(image)


# Dataset for training
dataset = mnist_ds.map(map_fn).batch(2).map(lambda x: tf.reduce_max(x, axis=0) / 255).map(lambda x: fun(x[None])[0])

# Initialzing generator and discriminator.
start_res = np.array([2, 4, 4])
end_res = np.array([16, 32, 32])
reses = [start_res, start_res * 2, end_res]  # all the resolutions represented in the GAN
try:
    # Load a discriminator and generator if model already exists
    with open("generator.pkl", "rb") as f:
        generator = pickle.load(f)
    with open("discriminator.pkl", "rb") as f:
        discriminator = pickle.load(f)
except:
# Initialize new network
    g_fmaps = [64, 64, 64, 64]
    # the discriminator needs one extra parameter for the fmaps as the last resolution also needs covnversion layer from RGB/grey to the network space
    d_fmaps = [64, 64, 64, 64, 64]
    generator = Generator(start_res, end_res, g_fmaps)
    discriminator = Discriminator(start_res, end_res, d_fmaps)

# Lists for analysis
dists_to_plot = []  # SWD every 100 steps
genned_hist = []  # generated samples every 100 steps
loss_hist = []  # loss every 100 steps
transitioning_hist = []  # Samples every 10 Minutes

z_dim = 100  # Define size of latent variable vector.
b_size = 4  # Batch Size
seed = tf.random.normal(shape=[b_size, z_dim])  # Seed for training analysis

# Lists needed for training as we change resolutions during training

# What resolution should be trained at epoch for generator
g_reses = [start_res, start_res, start_res*2, start_res * 2, start_res * 4, start_res *4, end_res]
 # What resolution should be trained at epoch for discriminator
d_reses = [start_res, start_res * 2, start_res * 2, start_res*4, start_res*4, end_res, end_res]
# Whether we are transitioning to next resolution (linear weighting)
transitioning = [False, True, False, True, False, True, False]
# factors to down-sample by for that resolution
factors = [8,4,4, 2, 2, 1, 1]
# number of training steps in epoch
num_take = [2000, 2000,2000, 2000, 2000, 1500, 1500]
# learning rates when training for the different resolutions (generator)
g_lr = [0.003, 0.003, 0.003, 0.003, 0.003, 0.002, 0.002]
# learning rates when training for the different resolutions (discriminator)
d_lr = [0.003, 0.003, 0.003, 0.003, 0.003, 0.002, 0.002]
start = time.time()
timer = start
for epochs in range(len(g_reses)):
    # initialize optimizers with custom learning rates for the current resolution
    gen_optimizer = tf.keras.optimizers.RMSprop(g_lr[epochs])
    dis_optimizer = tf.keras.optimizers.RMSprop(d_lr[epochs])
    step = 0
    # set all the custom values for the current resolution
    g_res = g_reses[epochs]
    d_res = d_reses[epochs]
    factor = factors[epochs]
    tran = transitioning[epochs]
    num_t = num_take[epochs]

    for real in dataset.batch(b_size).take(num_t):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as dis_tape:
            # weighting factor when transitioning to next resolution
            alpha = step / num_t

            # Generate random noise vector.
            noise = tf.random.normal((b_size, z_dim))
            # Generate fake images with generator.
            fake = generator(noise, g_res, alpha, tran)
            # Merge fake and real images to a long vector.
            d_real = layers.AveragePooling3D((factor, factor, factor))(real)
            images = tf.concat((fake, d_real), axis=0)

            # compute discriminator output for the samples
            output = discriminator(images, d_res, alpha, tran)

            # Generator loss.
            gen_loss = generator_loss(output[:b_size])

            # Unsymmetrical training as proposed in WGAN --> only update generator every n steps (n=1)
            if step % 5 == 0:
                gen_gradients = gen_tape.gradient(gen_loss, generator.trainable_variables)
                gen_optimizer.apply_gradients(zip(gen_gradients, generator.trainable_variables))

            # Discriminator loss with gradient penalty
            dis_loss = discriminator_loss(output[b_size:], output[:b_size])
            dis_loss = D_wgan_gp(dis_loss, discriminator, fake, b_size, d_real, d_res, alpha, tran)

            dis_gradients = dis_tape.gradient(dis_loss, discriminator.trainable_variables)
            dis_optimizer.apply_gradients(zip(dis_gradients, discriminator.trainable_variables))

            # loss history for plotting
            loss_hist.append([gen_loss, dis_loss])

        # Every 100 steps generate images from the defined seed and
        # stored to supervise how well the generator works.
        if step % 100 == 0 or step == num_t - 1:
            print(time.time() - start)  # check how long last 100 steps took

            # Generating samples from same seed for evaluation
            images = generator(seed, g_res, alpha, tran)

            # Printing image shape and loss to keep track of performance while training
            print(
                f'Epoch {epochs}, step {step}, resolution {images.shape},  gen_loss: {gen_loss.numpy()}, dis_loss: {dis_loss.numpy()},')
            for_met = layers.UpSampling3D((factor, factor, factor))(images)
            dists = compute_metric((real), for_met, reses, nhoods=2)
            dists_to_plot.append(dists)
            print(f"-----Wasserstein Distances: {dists}------")

            for sequence in images[:8]:
                sequence = np.squeeze(sequence, axis=-1)
                plt.figure(figsize=(20, 5))
                for i, img in enumerate(sequence):
                    plt.subplot(1, len(sequence), i + 1)
                    plt.imshow(img, cmap='gray')
                    plt.axis('off')
                plt.tight_layout()
                plt.show()
            # Storing all samples and then also every 10 minutes in a separate list to check performance over time
            genned_hist.append(images)
            if time.time() - timer >= 60:
                transitioning_hist.append(images)
                timer = time.time()
                print("TRANS_LIST:", len(transitioning_hist))
            with open("generator.pkl", "wb") as f:
                pickle.dump(generator, f)
            with open("discriminator.pkl", "wb") as f:
                pickle.dump(discriminator, f)
            with open("loss.pkl", "wb") as f:
                pickle.dump(loss_hist, f)
            with open("trans.pkl", "wb") as f:
                pickle.dump(transitioning_hist, f)

        step += 1
