"""@package docstring
Documentation for the Generator
"""
import numpy as np

from utils.layers.progan_layers import *
from utils.layers.stylegan_layers import *


class Generator:
    """
    The Generator as described in the thesis
    """

    def __init__(self, start_res, end_res, fmaps, num_channels=1):
        """
        Constructor for a generator
        @param start_res: lowest resolution that the network should be able to produce
        @param end_res: final resolution of the data
        @param fmaps: the number of feature maps for every block
        @param num_channels: number of channels found in the data (1 for grey-scale, 3 for rgb)
        """

        # Assertions if progressively Growing network do-able with given resolutions
        assert start_res.shape[0] == 3, "Network needs number of frames and frame size"
        assert end_res.shape[0] == 3, "Network needs number of frames and frame size"

        # compute number of necessary blocks to get from lowest to highest resolution
        num_blocks = np.log2(end_res[0]) - np.log2(start_res[0])
        assert num_blocks == np.log2(end_res[1]) - np.log2(start_res[1]), "number of necessary blocks should be the " \
                                                                          "same for frames and image size "
        assert num_blocks == np.log2(end_res[2]) - np.log2(start_res[2]), "number of necessary blocks should be the " \
                                                                          "same for frames and image size "
        assert num_blocks == len(fmaps) - 1, "Can not get from the start resolution to the final " \
                                             "resolution by doubling resolution multiple times "

        # store all possible Level of Details that the generator can produce for future assertions
        lods = [start_res]
        for mult in range(1, int(num_blocks)):
            lods.append(start_res * (2 ** mult))
        lods.append(end_res)
        lods = np.asarray(lods)

        self.blocks = []  # the blocks in the network containing the layers for every resolution
        self.trainable_variables = []  # list of all the variables that were used in one call of the generator
        self.to_rgbs = []  # Conv3D layer with kernel and step-size of 1 used for progressive training

        self.style_net = [
            layers.Dense(units=64),
            layers.Dense(units=64),
        ]  # style network for the latent space

        self.affine_trans = [
            [layers.Dense(fmaps[0]), layers.Dense(fmaps[0])],
            [layers.Dense(fmaps[0]), layers.Dense(fmaps[0])]
        ]  # list of the affine transformations for the style vectors used for AdaIN

        bytes_array = [arr.tobytes() for arr in lods]  # convert resolutions to bytes as array not hashable
        # dictionary of all the resolutions handled by the network so only valid resolutions can be generated
        self.lodict = dict(zip(bytes_array, list(range(1, len(lods) + 1))))

        # initial block containing the lowest resolution, must be handled as a special case as it starts of with a Dense
        # layer for the input
        self.blocks.append([layers.Dense(units=tf.math.reduce_prod(lods[0]) * fmaps[0], use_bias=False),
                            PixelNorm(),
                            layers.LeakyReLU(0.2),
                            layers.Reshape((lods[0][0], lods[0][1], lods[0][2], fmaps[0])),
                            AdaInstanceNormalization(),
                            layers.Conv3D(filters=fmaps[0], kernel_size=3, strides=1, padding='same', use_bias=False),
                            PixelNorm(),
                            layers.LeakyReLU(0.2),
                            AdaInstanceNormalization(),
                            Noise()
                            ])

        # Add necessary conversion layer as described in thesis
        self.to_rgbs.append(
            layers.Conv3D(filters=num_channels, kernel_size=1)
        )

        # Adding all the blocks that are necessary to generate the resolutions wanted
        for fmap in fmaps[1:]:
            self.add_blocks(fmap, num_channels)

    def add_blocks(self, fmap, num_channels=1, kernel_size=3):
        """
        Adds block to synthesis network, while also adding new affine transformation AdaIN
        @type fmap: number of feature maps for block that should be added to the network
        @param num_channels: number of channels found in the data
        @param kernel_size: size of the Conv3D kernel
        """
        # add block to network with given number of feature maps
        self.blocks.append([
            layers.UpSampling3D(2),
            layers.Conv3D(filters=fmap, kernel_size=kernel_size, strides=1, padding='same', use_bias=False),
            PixelNorm(),
            layers.LeakyReLU(0.2),
            AdaInstanceNormalization(),
            Noise()
        ])

        # Conversion layer
        self.to_rgbs.append(
            layers.Conv3D(filters=num_channels, kernel_size=1)
        )

        # affine transformations for AdaIn
        bias = layers.Dense(fmap)
        scale = layers.Dense(fmap)
        self.affine_trans.append([bias, scale])

    def __call__(self, style_inp, lod, alpha=0, transitioning=False):
        """
        Call function to generate sample using Generator
        @param style_inp: latent vector that is used for the style net
        @param lod: resolution the sample should have
        @param alpha: weighting when transitioning
        @param transitioning: boolean whether or not network is transitioning to next layer
        @return: Generated sample and list of used variables for gradient update
        """
        # list of variables that should be updated when the network is trained
        self.trainable_variables = []

        # number of blocks that are called for the wanted resolution
        num_blocks = self.lodict[lod.tobytes()]

        # calling style network for style vector
        stylevec = PixelNorm()(style_inp)
        for layer in self.style_net:
            stylevec = layer(stylevec)
            self.trainable_variables.extend(layer.trainable_variables)

        # Constant input
        x = constant_input(style_inp)
        ada_counter = 0 # keeps track which affine transformation needs to be used

        # calling layers of the network one by one
        for block in self.blocks[:num_blocks]:
            for layer in block:
                # check if layer is AdaIN to apply affine transformation
                if isinstance(layer, AdaInstanceNormalization):
                    aff_t = self.affine_trans[ada_counter]
                    ada_counter += 1
                    bias = layers.Reshape([1, 1, 1, aff_t[0].units])(aff_t[0](stylevec))
                    scale = layers.Reshape([1, 1, 1, aff_t[1].units])(aff_t[1](stylevec))
                    x = layer([x, bias, scale])
                    self.trainable_variables.extend(aff_t[0].trainable_variables)
                    self.trainable_variables.extend(aff_t[1].trainable_variables)
                else:
                    x = layer(x)
                self.trainable_variables.extend(layer.trainable_variables)
        output = self.to_rgbs[num_blocks - 1](x)
        self.trainable_variables.extend(self.to_rgbs[num_blocks - 1].trainable_variables)

        if transitioning:
            # linear transition to next layer take x before the to_rgb layer and take weighted sum and then map to rgb
            assert num_blocks < len(self.blocks), "Already at highest LoD"
            output = layers.UpSampling3D(2)(output)
            # call the layers of the new block
            for layer in self.blocks[num_blocks]:
                if isinstance(layer, AdaInstanceNormalization):
                    aff_t = self.affine_trans[ada_counter]
                    bias = layers.Reshape([1, 1, 1, aff_t[0].units])(aff_t[0](stylevec))
                    scale = layers.Reshape([1, 1, 1, aff_t[1].units])(aff_t[1](stylevec))
                    x = layer([x, bias, scale])
                    self.trainable_variables.extend(aff_t[0].trainable_variables)
                    self.trainable_variables.extend(aff_t[1].trainable_variables)  # check if can be cut into one line
                else:
                    x = layer(x)
                self.trainable_variables.extend(layer.trainable_variables)
            x = self.to_rgbs[num_blocks](x)
            self.trainable_variables.extend(self.to_rgbs[num_blocks].trainable_variables)

            # weighted sum of both outputs mapped to rgb
            output = output + (x - output) * alpha
        return output
