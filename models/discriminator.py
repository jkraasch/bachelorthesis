import numpy as np
from tensorflow.keras import layers
from utils.layers.progan_layers import *


class Discriminator():
    def __init__(self, start_res, end_res, fmaps):
        """
        Constructor for a generator
        @param start_res: lowest resolution that the network should be able to produce
        @param end_res: final resolution of the data
        @param fmaps: the number of feature maps for every block
        """
        # Assertions if progressively Growing network do-able with given resolutions
        assert start_res.shape[0] == 3, "Network needs number of frames and frame size"
        assert end_res.shape[0] == 3, "Network needs number of frames and frame size"

        # compute number of necessary blocks to get from lowest to highest resolution
        num_blocks = np.log2(end_res[0]) - np.log2(start_res[0])
        assert num_blocks == np.log2(end_res[1]) - np.log2(start_res[1]), "number of necessary blocks should be the " \
                                                                          "same for frames and image size "
        assert num_blocks == np.log2(end_res[2]) - np.log2(start_res[2]), "number of necessary blocks should be the " \
                                                                          "same for frames and image size "
        assert num_blocks == len(fmaps) - 2, "Can not get from the start resolution to the final " \
                                             "resolution by doubling resolution multiple times "

        # store all possible Level of Details that the generator can produce for future assertions
        lods = [start_res]
        for mult in range(1, int(num_blocks)):
            lods.append(start_res * (2 ** mult))
        lods.append(end_res)
        lods = np.asarray(lods)

        bytes_array = [arr.tobytes() for arr in lods]
        # dictionary of all the resolutions handled by the network so only valid resolutions can be generated
        self.lodict = dict(zip(bytes_array, list(range(len(lods)))))

        self.blocks = []  # the blocks in the network containing the layers for every resolution
        self.trainable_variables = []  # list of all the variables that were used in one call of the generator
        self.from_rgb_layers = []  # Conv3D layer with kernel and step-size of 1 used for progressive training

        # initial block containing the lowest resolution, must be handled as a special case as it ends of with a Dense
        # layer for the input and it also gets the MinibatchSTD fmap
        self.blocks.append([
            MinibatchStdev(),
            layers.Conv3D(filters=fmaps[0], kernel_size=3, strides=(2, 2, 2), padding='same'),
            layers.LeakyReLU(0.2),
            layers.Flatten(),
            layers.Dense(units=1),
        ])

        # Add necessary conversion layer as described in thesis
        self.from_rgb_layers.append(layers.Conv3D(filters=fmaps[1], kernel_size=1))

        # Adding all the blocks that are necessary to discriminate the resolutions wanted
        for i in range(1, len(fmaps) - 1):
            self.add_block(fmaps[i], fmaps[i + 1])

    def add_block(self, filters, prior_dim, kernel_size=3):
        """
        Adds block to the discriminator
        @param filters: Number of feature maps
        @param prior_dim: the number of fmaps from the previous layer for the rgb mapping
        @param kernel_size: size of the Conv3D kernel
        """

        # add block to network with given number of feature maps
        block = [
            layers.Conv3D(filters=filters, kernel_size=kernel_size, strides=(2, 2, 2), padding="same", use_bias=False),
            PixelNorm(),
            layers.LeakyReLU(0.2)
        ]

        # Conversion layer
        from_rgb = layers.Conv3D(filters=prior_dim, kernel_size=1)

        self.blocks.append(block)
        self.from_rgb_layers.append(from_rgb)

    def __call__(self, x, lod, alpha=0, transitioning=False):
        """
       Call function to generate sample using Generator
       @param x: video that should be evaluated
       @param lod: resolution the samples have
       @param alpha: weighting when transitioning
       @param transitioning: boolean whether or not network is transitioning to next layer
       @return: Discriminator output for the sample and list of used variables for gradient update
       """
        # list of variables that should be updated when the network is trained
        self.trainable_variables = []

        #input tmp storage if transitioning this is needed as it has to be down-sampled for the weighted sum
        images = x

        # number of blocks that are called for the wanted resolution
        num_blocks = self.lodict[lod.tobytes()]

        # Calling the Discriminator on the batch
        x = self.from_rgb_layers[num_blocks](x)
        self.trainable_variables.extend(self.from_rgb_layers[num_blocks].trainable_variables)
        for block in self.blocks[num_blocks::-1]:
            for layer in block:
                if isinstance(layer, MinibatchStdev):
                    x = layer(images, x)
                else:
                    x = layer(x)
                self.trainable_variables.extend(layer.trainable_variables)
        if transitioning:
            # linear transition to next layer
            assert num_blocks > 0, f"Already at lowest LoD: {num_blocks}"
            # down sample the original batch so it can be fed to the prior sub-model
            y = layers.AveragePooling3D((2, 2, 2))(images)
            y = self.from_rgb_layers[num_blocks - 1](y)
            self.trainable_variables.extend(self.from_rgb_layers[num_blocks - 1].trainable_variables)
            for block in self.blocks[num_blocks - 1::-1]:
                for layer in block:
                    if isinstance(layer, MinibatchStdev):
                        y = layer(images, y)
                    else:
                        y = layer(y)
                    self.trainable_variables.extend(layer.trainable_variables)
            #weighted sum
            x = y + (x - y) * alpha

        return x
